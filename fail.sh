#!/bin/bash

EXIT_CODE=$( curl --silent "http://www.randomnumberapi.com/api/v1.0/random?min=0&max=1&count=1" | cut -c2)
echo $EXIT_CODE

exit $EXIT_CODE
